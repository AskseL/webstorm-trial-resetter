#!/bin/sh

#variables
last_reset_timestamp_file=".last_reset_webstorm"
webstorm_run_script="/opt/webstorm/bin/webstorm.sh"
reset_period=25


last_time=`cat $last_reset_timestamp_file`
last_time=$(($last_time + 0))
current_time=`date +%s`
reset_period=$(($reset_period * 86400))
time_diff=$(($current_time - $last_time))
if [ "$time_diff" -gt "$reset_period" ]; then
	echo $current_time > $last_reset_timestamp_file
	# Reset WebStorm
	rm -rf ~/.config/JetBrains/WebStorm*/eval
	# rm -rf ~/.config/JetBrains/WebStorm*/options/other.xml
	sed -i -E 's/<property name=\"evl.*\".*\/>//' ~/.config/JetBrains/WebStorm*/options/other.xml
	rm -rf ~/.java/.userPrefs/jetbrains/webstorm
    eval "nohup ${webstorm_run_script} > /dev/null &"
else
    eval "nohup ${webstorm_run_script} > /dev/null &"
fi